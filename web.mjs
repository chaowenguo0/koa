import Koa from 'koa' 
import http from 'http' 
import {promises as fs} from 'fs' 
import cors from '@koa/cors' 
import body from 'koa-body' 
 
const app = new Koa 
app.use(cors()).use(body.koaBody({includeUnparsed:true})).use(async (ctx,next) =>  
{ 
    if (globalThis.Object.is(ctx.method, 'GET')) 
    { 
        ctx.body = await fs.readFile('public/index.html', 'utf-8') 
        ctx.type == 'text/html' 
    } 
    else await next() 
}).use(async (ctx,next) => 
{ 
    const body = ctx.request.body[globalThis.Symbol.for('unparsedBody')] 
    if (globalThis.Object.is(ctx.path, '/episode')) 
    { 
        ctx.body = await globalThis.fetch('https://huggingface.co/api/models/chaowenguo/video').then(_ => _.json()).then(_ => globalThis.Object.values(_.siblings.findLast(_ => globalThis.Object.values(_).at(0).includes(body))).at(0).split('/').at(-1).split('.').at(0)) 
        ctx.type = 'text/plain' 
    } 
    else if (globalThis.Object.is(ctx.path, '/list')) ctx.body = [...new globalThis.Set(await globalThis.fetch('https://huggingface.co/api/models/chaowenguo/video').then(_ => _.json()).then(_ => _.siblings.map(_ => globalThis.Object.values(_).at(0)).filter(_ => _.includes(body)).map(_ => _.split('/').at(1))))] 
    else await next() 
}).use(async ctx => 
{ 
    const {drama, episode} = globalThis.JSON.parse(ctx.request.body[globalThis.Symbol.for('unparsedBody')]) 
    if (globalThis.Object.is(ctx.path, '/mp4')) 
    { 
        ctx.body = await globalThis.fetch(`https://huggingface.co/chaowenguo/video/resolve/main/${drama}/${episode}.mp4`).then(_ => _.url) 
        ctx.type = 'text/plain' 
    } 
    else 
    { 
        ctx.body = await globalThis.fetch(`https://huggingface.co/chaowenguo/video/raw/main/${drama}/${episode}.vtt`).then(_ => _.text()) 
        ctx.type = 'text/plain' 
    } 
}) 
http.createServer(app.callback()).listen(3000)